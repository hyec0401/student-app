package com.tmobile.student.web.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.tmobile.student.backend.repository.StudentAddressRepository;
import com.tmobile.student.web.dto.StudentAddressDTO;

 

@Controller
public class StudentAddressController {

	@Autowired
	private StudentAddressRepository studentAddressRepository;
	
	
	@RequestMapping(value ="/")
	public String address(){
		return "address"; //return address.jsp 
	}
	
	
	@RequestMapping("/address")
	public String searchAddress(@RequestParam("studentNo") String studentNo, Map<String, Object> model){
		System.err.println("need to be number");
		
		List<StudentAddressDTO> saDTOs = studentAddressRepository.findStudentAddressByStudentNo(studentNo);
		model.put("AddressList", saDTOs);
				
		return "searchResult" ;
	}
	
	
	
}
