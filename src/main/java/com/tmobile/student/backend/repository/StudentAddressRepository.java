package com.tmobile.student.backend.repository;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.tmobile.student.backend.rowmapper.StudentAddressRM;
import com.tmobile.student.web.dto.StudentAddressDTO;


@Repository
public class StudentAddressRepository {

	 @Autowired
	 private JdbcTemplate jdbcTemplate;
	 
	 
	 public List<StudentAddressDTO> findStudentAddressByStudentNo(String studentNo) { 
		 
		String sql = "SELECT sa.*, s.student_no, s.first_name, s.last_name " + "FROM STUDENT_ADDRESS sa "
		              + "INNER JOIN STUDENT s ON s.id = sa.student_id and s.student_no like '%"    + studentNo +"%' "; 
	  
		List<StudentAddressDTO> results = jdbcTemplate.query(sql, new StudentAddressRM());
		return results;
	 }
    
	 
}
