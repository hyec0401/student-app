DROP TABLE IF EXISTS student;  
create table if not exists student(
                id bigint primary key auto_increment,
                student_no varchar(100), 
                first_name varchar(100), 
                last_name varchar(30));

DROP TABLE IF EXISTS student_address;  
create table if not exists student_address(
                id bigint primary key auto_increment,
                student_id bigint, 
                address varchar(100), 
                city varchar(30),
                state varchar(30),
                zip_code varchar(30));